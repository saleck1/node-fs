import express, {NextFunction, Request, Response} from "express";
import * as fs from "fs";
import * as jwt from 'jsonwebtoken';
import {Secret} from 'jsonwebtoken';


class Routers{
    constructor(public user = {
        id: 14,
        name: 'Bon JOUR',
        email: 'bonjour@gmail.com',
        admin: true,

    }) {
        this.path()

    }

    public route = express.Router({caseSensitive: true})

    private  path =(): void =>{
        this.generFile()
        this.readFile()
        this.readStream()
        this.loginToken()
    }

    private generFile():void {
        this.route.get('/gener',this.authentification,(req: Request, res: Response)=>{
            fs.appendFile(__dirname + '/nouveauFichier.txt', 'Mon contenu', function (err) {
                if (err) throw err;
                console.log('Fichier créé !');
                res.status(200).json('Fichier créé !')
            });
        })
    }

    private readFile():void{
        this.route.get('/read',this.authentification,(req: Request,res: Response)=>{
            fs.readFile(__dirname + '/sample-5s.mp4',(err,data)=>{
                if(err) throw err
                res.write(data);

                res.status(200).end();
            });


        })
    }

    private readStream():void{
        this.route.get('/stream',this.authentification,(req: Request,res: Response)=>{
            const stream = fs.createReadStream('sample-5s.mp4')
            stream.pipe(res)
        })
    }


        public generatAccess=(user: Object)=>{
            return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET as Secret,{expiresIn: '1800s'})
        }


        private loginToken=()=>{
            this.route.post('/login', (req: Request, res: Response) => {

                // TODO: fetch le user depuis la db basé sur l'email passé en paramètre
                if (req.body.email !== 'bonjour@gmail.com') {
                    res.status(401).send('invalid credentials');
                    return ;
                }
                // TODO: check que le mot de passe du user est correct
                if (req.body.password !== 'test') {
                    res.status(401).send('invalid credentials');
                    return ;
                }

                const accessToken = this.generatAccess(this.user);


                res.send({
                    accessToken,

                });

            });
        }

        private authentification = async (req: Request | any, res: Response, next: NextFunction) => {
            let token: any = req.headers['x-access-token'] || req.headers['authorization'];
            if (!!token && token.startsWith('Bearer ')) {
                token = token.slice(7, token.length);
            }

            if (token) {
                jwt.verify(token, process.env.ACCESS_TOKEN_SECRET as Secret, (err: any, decoded: any) => {
                    if (err) {
                        return res.status(401).json('token_not_valid');
                    } else {
                        req.decoded = decoded;

                        const newToken  = jwt.sign({
                                user : decoded.user
                            },
                            process.env.ACCESS_TOKEN_SECRET as Secret,
                            {
                                expiresIn: 1800
                            });

                        res.header('Authorization', 'Bearer ' + newToken);
                        next();
                    }
                });
            } else {
                return res.status(401).json('token_required');
            }
        }


















}
export default new Routers().route
